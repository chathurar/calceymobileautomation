export const DEFAULT_TIMEOUT = 11000;

export const IMAGE_COMPARISON_MODE = {
  MATCH_FEATURES_MODE: 'matchFeatures',
  GET_SIMILARITY_MODE: 'getSimilarity',
  MATCH_TEMPLATE_MODE: 'matchTemplate',
};
