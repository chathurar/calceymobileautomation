import BaseAppScreen from './base-app.screen';

const SELECTORS = {
  SignIn_BUTTON: '//*[@text="SIGN IN"]',
  Email: `//android.view.ViewGroup[1]/android.widget.EditText`,
  Password: `//android.view.ViewGroup[2]/android.widget.EditText`,
};

export default class LoginScreen extends BaseAppScreen {
  constructor() {
    super();
    $(SELECTORS.Email).waitForDisplayed();
  }

  setUsername(username: string): void {
    $(SELECTORS.Email).click();
    $(SELECTORS.Email).setValue(username);
  }

  setPassword(password: string): void {
    $(SELECTORS.Password).click();
    $(SELECTORS.Password).setValue(password);
  }

  clickLogin(): void {
    $(SELECTORS.SignIn_BUTTON).click();
  }

  login(username: string, password: string): void {
    this.setUsername(username);
    this.setPassword(password);
    this.clickLogin();
  }
}
