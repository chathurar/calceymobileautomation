import { readFileSync } from 'fs';
import { IMAGE_COMPARISON_MODE } from '../constants';

export default class BaseAppScreen {
  ScrollPage(startingX: number, startingY: number, endX: number, endY: number): void {
    browser.touchPerform([
      { action: 'longPress', options: { x: startingX, y: startingY } },
      { action: 'moveTo', options: { x: endX, y: endY } },
      { action: 'release' },
    ]);
  }

  tapOnScreen(X: number, Y: number): void {
    browser.touchAction({
      action: 'tap',
      x: X,
      y: Y,
    });
  }

  imageComparison(baseImageName: string, screenshotName: string, scoreNumber?: number): void {
    const folderName = 'emulator'; //mobile
    const baselineImageBase64 = readFileSync(`./images/base/${folderName}/${baseImageName}.png`, { encoding: 'base64' });
    const screenShotBase64 = readFileSync(`./images/screenshots/${screenshotName}.png`, { encoding: 'base64' });

    const result = driver.compareImages(
      IMAGE_COMPARISON_MODE.GET_SIMILARITY_MODE,
      baselineImageBase64,
      screenShotBase64,
      {},
    );

    expect(result).not.toBeNull();
    expect(result.score).not.toBeNull();

    if (scoreNumber) {
      expect(result.score).toBeGreaterThan(scoreNumber);
    } else {
      expect(result.score).toBeGreaterThan(0.99);
    }
  }

  takeScreenShot(imageName: string, pauseTime?: number): void {
    const saveScreenshotFilepath = `./images/screenshots/`;
    if (pauseTime) {
      browser.pause(pauseTime * 1000);
    } else {
      browser.pause(3000);
    }
    browser.saveScreenshot(saveScreenshotFilepath + imageName + '.png');
  }

  takeScreenShotAndCompare(imageName: string, pauseTime?: number, scoreNumber?: number): void {
    this.takeScreenShot(imageName, pauseTime);
    this.imageComparison(imageName, imageName, scoreNumber);
  }
}
