import LoginScreen from '../screenobjects/login.screen';

describe('Login to the System', () => {
  let loginScreen: LoginScreen;

  beforeAll(() => {
    loginScreen = new LoginScreen();
  });

  it(`Verify screenshot Login Screen`, () => {
    loginScreen.takeScreenShotAndCompare('LoginPage');
  });

  it('login', () => {
    loginScreen.login('chathura', 'user@123');
  });
});
