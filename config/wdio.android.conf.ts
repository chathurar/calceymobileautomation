import config from './wdio.shared.conf';
import { join } from 'path';

// ============
// Specs
// ============
config.specs = ['./tests/specs/*.spec.ts'];

// ============
// Capabilities
// ============
// For all capabilities please check
// http://appium.io/docs/en/writing-running-appium/caps/#general-capabilities
const capabilities: WebDriver.DesiredCapabilities = {
  platformName: 'Android',
  app: join(process.cwd(), './apps/scout.apk'),
  deviceName: 'emulator-5554',
  platformVersion: '10',
  automationName: 'UiAutomator2',
  autoGrantPermissions: true,
  maxInstances: 1,
};

config.capabilities = [capabilities];

export { config };
