import config from './wdio.shared.conf';
import { join } from 'path';

// ============
// Specs
// ============
config.specs = ['./tests/specs/**/*.ts'];

// ============
// Capabilities
// ============
// For all capabilities please check
// http://appium.io/docs/en/writing-running-appium/caps/#general-capabilities
const capabilities: WebDriver.DesiredCapabilities = {
  platformName: 'iOS',
  app: join(process.cwd(), './apps/iOS-NativeDemoApp-0.2.0.zip'),
  deviceName: 'iPhone X',
  platformVersion: '14.1',
  automationName: 'XCUITest',
};

config.capabilities = [capabilities];

export { config };
