## Development environment

- Recommended to install nvm to manage node version
- Use node v14.15.0 for development
- install node modules with `npm install`

## WDIO Docs
- https://webdriver.io/docs/api.html

## install appium
npm install appium -g

## install nodemodules (install all lib)
npm install

## run test
npm run android

## run allure reports
npm run allure
